from django import forms
from .models import MataKuliah


class MatkulForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = [
            'namaMatkul',
            'dosenPengajar',
            'sks',
            'semester',
            'kelas',
            'deskripsi'
        ]

        widgets = {
            'namaMatkul': forms.TextInput(attrs={'class': 'form-control'}),
            'dosenPengajar': forms.TextInput(attrs={'class': 'form-control'}),
            'sks': forms.TextInput(attrs={'class': 'form-control'}),
            'semester': forms.TextInput(attrs={'class': 'form-control'}),
            'kelas': forms.TextInput(attrs={'class': 'form-control'}),
            'deskripsi': forms.Textarea(attrs={'class': 'form-control'}),
        }
