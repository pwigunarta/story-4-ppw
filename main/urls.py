from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('CV/', views.cv, name='cv'),
    path('mataKuliah/accepted/', views.accepted, name='accepted'),
    path('story1/', views.story1, name='story1'),
    path('mataKuliah/table', views.data_list, name='list'),
    path('mataKuliah/detail/<int:id>/', views.data_detail, name='data detail'),
    path('mataKuliah/delete/<int:id>/', views.data_delete, name='data delete'),
    path('form/', views.form, name='form')

]
