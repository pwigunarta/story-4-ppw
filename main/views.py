from django.shortcuts import render, redirect
from .models import MataKuliah
from .forms import MatkulForm


def home(request):
    return render(request, 'main/home.html')


def cv(request):
    return render(request, 'main/cv.html')


def story1(request):
    return render(request, 'main/story1.html')


def accepted(request,):
    return render(request, 'main/accepted.html')


def form(request):
    formMatkul = MatkulForm(request.POST or None)
    if formMatkul.is_valid():
        formMatkul.save()
        return redirect('/mataKuliah/accepted/', )

    context = {
        'formMatkul': formMatkul
    }
    return render(request, 'main/form.html', context)


def data_list(request):
    obj = MataKuliah.objects.all()

    context = {
        'obj': obj
    }
    return render(request, "main/list.html", context)


def data_delete(request, id):
    obj = MataKuliah.objects.get(id=id)
    if request.method == "POST":
        obj.delete()
        return redirect('/mataKuliah/table')
    context = {
        'object': obj
    }
    return render(request, "main/delete.html", context)

def data_detail(request, id):
    obj = MataKuliah.objects.get(id=id)
    context = {
        'object' : obj
    }
    return render(request, 'main/detail.html', context)
