from django.db import models
from django.urls import reverse

# Create your models here.


class MataKuliah(models.Model):
    namaMatkul = models.CharField(max_length=100)
    dosenPengajar = models.CharField(max_length=100)
    sks = models.CharField(max_length=100)
    semester = models.CharField(max_length=100)
    kelas = models.CharField(max_length=100)
    deskripsi = models.TextField()

    def __str__(self):
        return self.namaMatkul

    def get_detail_url(self):
        return f"/mataKuliah/detail/{self.id}/"

    def get_delete_url(self):
        return f"/mataKuliah/delete/{self.id}/"
