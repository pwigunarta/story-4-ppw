from django.shortcuts import render, redirect
from .models import Acara, Peserta
from .forms import AcaraForm, PesertaForm

# Create your views here.


def register(request, id):
    form = PesertaForm(request.POST or None)
    if(form.is_valid()):
        personInEvent = Peserta(acaraDiikuti=Acara.objects.get(
            id=id), namaPeserta=form.data['namaPeserta'])
        personInEvent.save()
        return redirect('/story6/acara')

    context = {
        'form': form
    }
    return render(request, "register.html", context)


def tambahKegiatan(request):
    form = AcaraForm(request.POST or None)
    if(form.is_valid()):
        form.save()
        return redirect('/story6/acara')

    context = {
        'form': form
    }
    return render(request, "newEvent.html", context)


def listKegiatan(request):
    person = Peserta.objects.all()
    acara = Acara.objects.all()
    if(request.method == 'POST'):
        Peserta.objects.get(id=request.POST['id']).delete()
        return redirect('/story6/acara')

    context = {
        'person': person,
        'acara': acara
    }
    return render(request, "list.html", context)
