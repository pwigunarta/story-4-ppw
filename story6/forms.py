from django import forms
from .models import Peserta, Acara


class AcaraForm(forms.ModelForm):
    class Meta:
        model = Acara
        fields = [
            'namaAcara',
        ]

        widgets = {
            'nama Acara': forms.TextInput(attrs={'class': 'form-control'})
        }


class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = [
            'namaPeserta',
        ]

        widgets = {
            'nama Peserta': forms.TextInput(attrs={'class': 'form-control'})
        }
