from django.urls import path

from . import views

app_name = 'main'

urlpatterns= [
    path('peserta/register/<int:id>/', views.register, name='register'),
    path('acara/register/', views.tambahKegiatan, name = 'tambah-acara'),
    path('acara/', views.listKegiatan, name = "list-kegiatan" )
]