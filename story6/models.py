from django.db import models

# Create your models here.


class Acara(models.Model):
    namaAcara = models.CharField(max_length=100)


class Peserta(models.Model):
    namaPeserta = models.CharField(max_length=100)
    acaraDiikuti = models.ForeignKey(
        Acara, default=None, on_delete=models.CASCADE)