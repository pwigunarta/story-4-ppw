from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Acara, Peserta

# Create your tests here.


class ViewsTest(TestCase):

    def setUp(self):
        Acara.objects.create(namaAcara="Makan-Makan")
        Acara.objects.create(namaAcara="Belajar PPW")
        Peserta.objects.create(
            namaPeserta="Izuri Sang Petani Boba", acaraDiikuti=Acara.objects.all()[0])
        Peserta.objects.create(
            namaPeserta="Abdul sang Invoker", acaraDiikuti=Acara.objects.all()[0])

    def test1_addPerson(self):
        jumlahOrang = Peserta.objects.all().count()
        Peserta.objects.create(namaPeserta="Seralic",
                               acaraDiikuti=Acara.objects.all()[0])
        self.assertEqual(Peserta.objects.all().count(), jumlahOrang + 1)

    def test2_addAcara(self):
        jumlahAcara = Acara.objects.all().count()
        Acara.objects.create(namaAcara="Tidur")
        self.assertEqual(Acara.objects.all().count(), jumlahAcara + 1)

    def test3_hapusOrang(self):
        response = self.client.post('/story6/acara/', data={'id': 1})
        self.assertEqual(response.status_code, 302)

    def test4_addPerson_form(self):
        response = Client().post('/story6/peserta/register/1/', data={'namaPeserta': 'Fikmal'})
        self.assertEqual(response.status_code, 302)

    def test5_addAcara_form(self):
        response = Client().post('/story6/acara/register/', data={'namaAcara': 'Mengerjakan Story'})
        self.assertEqual(response.status_code, 302)

    def test6_addPerson_template(self):
        response = Client().get('/story6/peserta/register/1/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'register.html')

    def test7_addAcara_template(self):
        response = Client().get('/story6/acara/register/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'newEvent.html')

    def test8_listKegiatan_template(self):
        response = Client().get('/story6/acara/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'list.html')